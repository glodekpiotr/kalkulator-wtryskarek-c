#include <iostream>

using namespace std;

int main()
{
    int ilosc_sztuk;
    int ilosc_w_cyklu;
    int czas_cyklu;
    int czas_w_sekundach;

    cout <<"Kalkulator wtryskarek" << endl;

    cout <<"Ilosc sztuk w kartonie: ";
    cin >>ilosc_sztuk;

    cout <<"Ilosc czesci w cyklu: ";
    cin >>ilosc_w_cyklu;

    cout << "czas cyklu: ";
    cin >> czas_cyklu;

    czas_w_sekundach=(ilosc_sztuk/ilosc_w_cyklu)*czas_cyklu;

    int godzin;
    int minut;
    int reszta;

    minut=czas_w_sekundach/60;
    godzin=minut/60;
    reszta=minut%60;
    //Oczekiwanym wynikiem dla wartosci minut=70 bedzie 1h i 10 min.

    if( minut < 60 )
    {
        cout << "Na wyprodukowanie calego kartonu czesci potrzebujemy " << reszta << " minut" <<endl;
    }

    else
    {
        cout << "Na wyprodukowanie calego kartonu czesci potrzebujemy " << godzin << " godzin i " << reszta << " minut" <<endl;
    }


    float kartony=480.00/minut;
    //480.00 to wartosc dla 8h pracy (cala zmiana)

    cout << "Ilosc kartonow wyprodukowana na calej zmianie: "<< kartony <<endl;
    //Oczekiwany wynik ma byc z wartoscia po przecinku (o ile taka wartosc wystepuje) dzieki temu wiemy ze wyprodukujemy niecale 6 kartonow kiedy wynik wynosi 5,890

    return 0;
}
